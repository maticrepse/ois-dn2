var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesloKanala = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', '');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    trenutniKanal[socket.id] = 'Skedenj';
    //izpisUporabnikov(socket, 'Skedenj');
    //izpisUporabnikov(socket, trenutniKanal[socket.id]);
    socket.on('uporabniki', function(){
      socket.emit('uporabniki', izpisUporabnikov(socket, trenutniKanal[socket.id]));
    });
    obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function izpisUporabnikov(socket, kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var tabela = [];
  var stevec = 0;
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    tabela[stevec] = vzdevkiGledeNaSocket[uporabnikSocketId];
    stevec++;
  }
  return tabela;
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}


function pridruzitevKanalu(socket, kanal, geslo) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  gesloKanala[kanal] = geslo;
  socket.emit('pridruzitevOdgovor', {
    kanal: kanal,
    vzdevek: vzdevkiGledeNaSocket[socket.id]
  });
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  izpisUporabnikov(socket, kanal);
}

function najdiPrejemnika(prejemnik, kanali, vzdevkiGledeNaSocket){
  for(var kanal in kanali){
    kanal = kanal.substring(1, kanal.length);
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if(prejemnik == vzdevkiGledeNaSocket[uporabnikSocketId]){
         return uporabnikSocketId;
      }
    }
  }
}

function obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki){
  socket.on('zasebnoSporociloZahteva', function(zasebno){
    zasebno = zasebno.replace("\"", "");
    var prejemnik = zasebno.substring(0, zasebno.indexOf("\""));
    zasebno = zasebno.replace(prejemnik+"\" ", "");
    zasebno = zasebno.replace("\"", "");
    var zasebnoSporocilo = zasebno.substring(0, zasebno.length-1);
    if (uporabljeniVzdevki.indexOf(prejemnik)!=-1){
      if(prejemnik==vzdevkiGledeNaSocket[socket.id]){
        socket.emit('zasebnoSporociloOdgovor', {
          uspesno: false,
          sporocilo: 'Sporočila ' + zasebnoSporocilo + ' uporabniku z vzdevkom '+ prejemnik +' ni bilo mogoče posredovati.'
        });
      } else {
        var kanali = io.sockets.manager.rooms;
        var uporabnikSocketId = najdiPrejemnika(prejemnik, kanali, vzdevkiGledeNaSocket);
        io.sockets.socket(uporabnikSocketId).emit('sporocilo',{
          besedilo: vzdevkiGledeNaSocket[socket.id] + '(zasebno): ' + zasebnoSporocilo
        });
        socket.emit('zasebnoSporociloOdgovor', {
          uspesno: true,
          //sporocilo: 'Uporabniku '+prejemnik+' ste poslali zasebno sporočilo: '+ zasebnoSporocilo
          sporocilo: '(zasebno za '+prejemnik+'): '+zasebnoSporocilo
        });
        /*socket.broadcast.emit('sporocilo', {
          prejemnik: prejemnik,
          zasebnoSporocilo: zasebnoSporocilo,
          uporabnikSocketId: uporabnikSocketId
        });*/
      }
    }else{
      socket.emit('zasebnoSporociloOdgovor', {
        uspesno: false,
        sporocilo: 'Sporočila ' + zasebnoSporocilo + ' uporabniku z vzdevkom '+ prejemnik +' ni bilo mogoče posredovati.'
      });
    }
  }); 
}
function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var kanali = io.sockets.manager.rooms;
    var obstaja = false;
    var novKanal;
    var geslo;
    var iskani;
    if(kanal.novKanal.indexOf("\"")==0){
      kanal.novKanal = kanal.novKanal.replace("\"","");
      novKanal = kanal.novKanal.substring(0, kanal.novKanal.indexOf("\""));
      kanal.novKanal = kanal.novKanal.replace(novKanal+"\"");
      kanal.novKanal = kanal.novKanal.replace("\"", "");
      geslo = kanal.novKanal.substring(0, kanal.novKanal.length-1);
      for (var i in kanali){
        i = i.substring(1, i.length);
        if(novKanal == i){
          obstaja = true;
          iskani = i;
        }
      }
    }else{
      novKanal = kanal.novKanal;
      geslo="";
      for (var i in kanali){
        i = i.substring(1, i.length);
        if(novKanal == i){
          obstaja = true;
          iskani = i;
        }
      }
    }
    if(!obstaja){
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, novKanal, geslo);
    }else{
      if (geslo==gesloKanala[iskani]){
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, novKanal, geslo);
      }else{
        if(gesloKanala[iskani]==""){
          socket.emit('sporocilo', {
            besedilo: "Izbrani kanal "+ iskani +" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev "+iskani +" ali zahtevajte kreiranje kanala z drugim imenom."
          });
        }else{
          socket.emit('sporocilo', {
            besedilo: "Pridružitev v kanal "+iskani+" ni bilo uspešno, ker je geslo napačno!"
          });
        }
      }
    }
  });
}

/*function vrniSocketId(kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  for(var i in uporabnikiNaKanalu){
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    return uporabnikSocketId;
  }
}*/

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}