function narediSmajli(smajli, slika, sporocilo){
  while (sporocilo.indexOf(smajli)!=-1) {
      sporocilo = sporocilo.replace(smajli, "<img src= \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/"+slika+".png\"/>");
    }
  return  sporocilo;
}

function filtriraj(sporocilo){
  var xmlDoc=getXML().getElementsByTagName("word");
  for(var i=0; i<xmlDoc.length;i++){
    var grdaBeseda = xmlDoc[i].textContent;
    sporocilo = filtrirajGrdeBesede(sporocilo, " "+grdaBeseda+" ");
    //sporocilo = filtrirajGrdeBesede(sporocilo, grdaBeseda);
  }
  return sporocilo;
  
}

function pretvoriVSmajli(sporocilo){
  if(sporocilo.indexOf(";)")!=-1||sporocilo.indexOf(":)")!=-1||sporocilo.indexOf("(y)")!=-1||sporocilo.indexOf("(Y)")!=-1||sporocilo.indexOf(":*")!=-1||sporocilo.indexOf(":(")!=-1){
    while(sporocilo.indexOf("<")!=-1){
      sporocilo=sporocilo.replace("<", "&lt");
    }
    sporocilo=narediSmajli(";)", "wink", sporocilo);
    sporocilo=narediSmajli(":)", "smiley", sporocilo);
    sporocilo=narediSmajli("(y)", "like", sporocilo);
    sporocilo=narediSmajli("(Y)", "like", sporocilo);
    sporocilo=narediSmajli(":*", "kiss", sporocilo);
    sporocilo=narediSmajli(":(", "sad", sporocilo);
    return $('<div style="font-weight: bold"></div>').append(sporocilo);
  }else{
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}

function divElementEnostavniTekst(sporocilo) {
  sporocilo = filtriraj(sporocilo);
  return pretvoriVSmajli(sporocilo);
}
//burek
function getXML() {
    var XML = null;
    $.ajax({
        url:      'swearWords.xml',
        dataType: 'xml',
        async:    false,
        success:  function(data) {
            XML = data;
        }
    });
    return XML;
}
function filtrirajGrdeBesede(sporocilo, grdaBeseda){
  sporocilo = " "+sporocilo+" ";
  var original=sporocilo;
  sporocilo = sporocilo.toLowerCase();
  grdaBeseda = grdaBeseda.toLowerCase();
  sporocilo=sporocilo.replace(/[^a-ž0-9A-Ž ]/g, " ");
  while(sporocilo.indexOf(grdaBeseda)!=-1){
    var zvezdice = "";
    for(var i=0; i<grdaBeseda.length-2;i++){
      zvezdice =zvezdice + "*";
    }
    zvezdice = zvezdice +"";
    //original=original.substring(0, sporocilo.indexOf(grdaBeseda)+1)+ zvezdice + original.substring((sporocilo.indexOf(grdaBeseda)+1)+zvezdice.length, original.length);
    //sporocilo=sporocilo+"100 ";
    original = original.substring(0, sporocilo.indexOf(grdaBeseda)+1)+zvezdice+original.substring(sporocilo.indexOf(grdaBeseda)+zvezdice.length+1, original.length);
    sporocilo = sporocilo.substring(0, sporocilo.indexOf(grdaBeseda)+1)+zvezdice+sporocilo.substring(sporocilo.indexOf(grdaBeseda)+zvezdice.length+1, sporocilo.length);
    //sporocilo = sporocilo.replace(grdaBeseda, zvezdice);
  }
  sporocilo=original;
  sporocilo = sporocilo.substring(1, sporocilo.length-1);
  return sporocilo;
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
//Pogleda, ce je sporocilo, ali zahteva.
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var kanal = $('#kanal').text();
    kanal = kanal.substring(kanal.indexOf("@")+2, kanal.length);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}
//entry point
var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  socket.on('zasebnoSporociloOdgovor', function(rezultat) {
      var sporocilo;
      if(rezultat.uspesno){
        sporocilo=rezultat.sporocilo;
      }else{
        sporocilo=rezultat.sporocilo;
      }
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
  });
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + " @ " +rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " +rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  socket.on('uporabniki', function(uporabniki) {
      $('#seznam-uporabnikov').empty();
      for(var i = 0; i<uporabniki.length;i++){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
  })
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});